# 4.3.1 Optische (aktiv) Triangulation

![aktiv triangulation](./Sources/aktivTriangulation.PNG)

* Laser
* Optische Sensor

**Distance objet** : $d$

```math
\begin{aligned}
d_{min} = [T1, R1]\\
d_{max} = [T2, R1]
\end{aligned}
```

**Distance Lazer - capteur** :  $b$

**angle d'arrivé sur le capteur** : $\varphi$

```math
\begin{aligned}
    \dfrac{d}{b}=tan \varphi \\
    d = b \cdot tang \varphi
\end{aligned}
```

## Sensibilité

```math
\begin{aligned}
    \dfrac{\delta \varphi}{\delta d}\\
    \dfrac{\delta d}{\delta \varphi} = b \cdot \dfrac{1}{\cos^2 \varphi}\\
    \delta \varphi = \dfrac{\cos^2\varphi}{b} \cdot \delta d\\
\end{aligned}
```
mit :

```math
\begin{aligned}
    x^2 = d^2 + b^2 \\
    \cos \varphi = \dfrac{b}{x} \\
\end{aligned}
```

Donne :
```math
\begin{aligned}
    \cos^2 \varphi = \dfrac{b^2}{x^2} = \dfrac{b^2}{d^2 + b^2}
\end{aligned}
```

Donc :
```math
\begin{aligned}
    \delta \varphi = \dfrac{b}{d^2 + b^2} \cdot \delta d\\
\end{aligned}
```