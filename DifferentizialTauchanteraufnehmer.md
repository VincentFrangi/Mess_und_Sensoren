# 4.1.2 Differentizial-Tauchanteraufnehmer

Betrieb mit Wechelspannungsbrücke

[//]: # (insert pic)
**Wechselspannungausschlagbrücke**

```math
\begin{aligned}
    Z_L = R_L + jwl \\
    R_L = const \\
    U_D = \dfrac{R_2 \cdot R_3 - R1 \cdot R4}{(R_1 + R_2)(R_3 + R_4)} 
    = \dfrac{Z_2 \cdot Z_3 - Z1 \cdot Z4}{(Z_1 + Z_2)(Z_3 + Z_4)} 
\end{aligned}
```

mit
```math
Z_
```
