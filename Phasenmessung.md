# 4.3.3 Phasenmessung

![sinus](./sources/sinus.png)

```math
\begin{aligned}
\omega = \dfrac{\Delta \varphi}{\Delta t } \\
\omega = 2 \cdot \pi \cdot f_{mod} \\
\Delta t = \dfrac{2d}{c}
\end{aligned}
```
d = Abstand Sender Reflector

```math
\begin{aligned}
2 \cdot \pi \cdot f_{mod} = \dfrac{\Delta \varphi}{\Delta t } = \dfrac{c}{2d} \cdot  \Delta\varphi \\
d = \dfrac{c \Delta \varphi}{4 \pi \cdot f_{mod}}
\end{aligned}
```

Eindentigkeitsbereich :
* $\pi$ : ExclusivOR-Schaltung
* $2\pi$ : vorzeichen-PWM

bei EXOR
```math
\begin{aligned}
\Delta f_{max} = \pi \\
d_{max} = \dfrac{c}{4 \cdot f_{Mod}}
\end{aligned}
```

Beispier für $f_{mod} = 8$MHz
```math
\begin{aligned}
d_{max}(8MHz) = \dfrac{2.9979 \cdot 10^8 m \cdot s^{-1}}{4 \cdot 8 \cdot 10^6 s^{-1}} = 9.375m 
\end{aligned}
```
