# 6.3 Kleinstes-Quadrate-Schätzer

Formalisierung : Gegeben dans Modell mit m wahren Paramettern

```math
\begin{aligned}
    \underline{y} = (y_1, y_2, ... , y_m)^T \\
    \underline{x} = (x_1, x_2, ... , x_m)^T
\end{aligned}
```

Hat man n ideale Beobachteungen werden diese im linearen Fall über eine Verschrift verknüpfent

```math
\begin{aligned}
    \underline{x} = H \cdot y \\
    H = \text{ Beobartungmatrix}
\end{aligned}
```

Erfolgen Messungen gibt :

```math
\begin{aligned}
   e = \Delta x = \hat{x} - x \\
   x = \hat{x} - e \\
   \underline{\hat{x}} - \underline{e} = H \cdot \underline{y} \\
   \underline{e} = \underline{\hat{x}} - H \cdot \underline{y}
\end{aligned}
```

Die wahren Parameter sind : i. d. R ebenfalls unbekannt, so dass sie durch ihrem Schätzer $\hat{y} = (\hat{y}_1, \hat{y}_2, ... , \hat{y}_m)^T$ erschtzt werden

```math
\begin{aligned}
   \underline{\hat{e}} = \underline{\hat{x}} - H \cdot \underline{\hat{y}}
\end{aligned}
```

ln diser Gleichung repräsentirt $\underline{\hat{e}}$ den Residenvektor

Unser Schätzer (KQS) minimiert die Summe den Residenquadrate !

Guten mass:

```math
\begin{aligned}
   J(\underline{\hat{y}}) = \sum_i^m \hat{e}_i^T = \underline{\hat{e}}^T \underline{\hat{e}}
\end{aligned}
```



```math
\begin{aligned}
   J(\underline{\hat{y}}) &= \underline{\hat{e}}^T \cdot \underline{\hat{e}}
   = (\underline{\hat{x}} - H \cdot \underline{\hat{y}}) ^T
   \cdot (\underline{\hat{x}} - H\cdot \underline{\hat{y}})
   \text{ mit: } (H \cdot \hat{y})^T = \hat{y}^T \cdot H^T \\

   &= \underline{\hat{x}}^T \cdot \underline{\hat{x}}
    \underbrace{
        - \underline{\hat{x}}^T \cdot H \underline{\hat{y}}
        - \underline{\hat{y}}^T H^T \cdot \underline{\hat{x}}
   }_{-2 \underline{\hat{x}}^T H \underline{\hat{y}}}
   + \underline{\hat{y}}^T H^T H \underline{\hat{y}}
\end{aligned}
```

Optimalen Parameter $\underline{\hat{y}}$

```math
\begin{aligned}
    \dfrac{\delta J(\underline{\hat{y}})}{\delta \underline{\hat{y}}}
    = \dfrac{\delta}{\delta \underline{\hat{y}} }
    \bigg[
        \underline{\hat{x}}^T \underline{\hat{x}}
        -2\underline{\hat{x}}^T H \underline{\hat{y}}
        + \underline{\hat{y}}^T H^T H \underline{\hat{y}}
    \bigg] 
    \stackrel{!}{=} 0
\end{aligned}
```

```math
\begin{aligned}
    \dfrac{\delta \underline{\hat{\sigma}}^T \underline{y} }{\delta \underline{y}}
    = \dfrac{\delta \underline{\hat{y}}^T \underline{v} }{\delta \underline{y}}
    = \underline{v}
\end{aligned}
```

```math
\begin{aligned}
    \dfrac{\delta \underline{\hat{y}}^T A \underline{y} }{\delta \underline{y}}
    = 2 H\underline{y}
\end{aligned}
```

```math
\begin{aligned}
    \dfrac{\delta J ( \underline{\hat{y}}^T) }{\delta \underline{y}}
    = - 2(\underline{\hat{x}}^T H)^T
    + 2 H^T H \underline{\hat{y}}
    = - 2 H^T \underline{\hat{x}}
    + 2 H^T H \underline{\hat{y}}
    \stackrel{!}{=} 0
\end{aligned}
```

```math
\begin{aligned}
    H^T H \hat{y} = H^T \cdot \underline{\hat{x}}  \\
    \boxed{
        \underline{\hat{y}} = (H^T H)^{-1}
        \cdot H^T \cdot \underline{\hat{x}}
    }
\end{aligned}
```

## Bsp Tennisball

```math
\begin{aligned}
   s = 0 \\
   s = 1 \\
   s = 2 \\
   s = 3
\end{aligned}
```


```math
\begin{aligned}
    s(t) &= 1 \space 2 \space  3 \space  4 \\
    \hat{v(t)} &= 55 \space 54 \space 53 \space 53
\end{aligned}
```

**Modell**:

```math
\begin{aligned}
    s(t) = \dfrac{v_0}{\alpha} \cdot (1 -e^{-\alpha t})
\end{aligned}
```

### 1. 
Gesuch $ \underline{y} = [v_o \alpha]^T$ $\underline{\hat{x}} = \underline{\hat{v(t)}}$

Modell unbannen um H zu ..

```math
\begin{aligned}
   \dfrac{\delta s(t)}{\delta t} = v(t)
   = - \dfrac{v_0}{\alpha} \cdot (- \alpha e^{-\alpha t}) \\
   s(t) = \dfrac{\hat{v_0}}{\alpha} - \dfrac{v_0}{\alpha} \cdot e^{-\alpha t}
   = \dfrac{v_0}{\alpha} - \dfrac{v(t)}{\alpha} \\
\end{aligned}
```

mit
```math
\begin{aligned}
   \underline{\hat{x}} = \underline{\hat{v}}(t)
\end{aligned}
```

und
```math
\begin{aligned}
   \underline{\hat{v}}(t) = v_0 - \alpha ..
\end{aligned}
```