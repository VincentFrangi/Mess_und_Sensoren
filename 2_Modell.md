# 2. Modell

```math
\begin{aligned}
     	\upsilon_t = k \cdot e^{-\alpha t} \qquad \text{Nicht immer}
\end{aligned}
```


```math
\begin{aligned}
    \ln \upsilon_t &= \ln(k \cdot e^{-\alpha t})
    \qquad
    \text{gesucht k und $\alpha$} \\
    
    \underbrace{\ln \upsilon_t}_{\hat{x}} 
    &= \ln k + \ln e^{-\alpha t} 
    = \underbrace{\ln k}_{\hat{y_1}} 
    -\underbrace{\alpha t}_{\hat{y_2}}
\end{aligned}
```

**2 Schaätzer:**
```math
\begin{aligned}
     H \cdot \underline{y} + \underline{e} 
     &= \underline{\hat{x}} \\

     \begin{bmatrix}
        1 - t_1 \\
        1 - t_2 \\
        1 - t_3 \\
        ... \\
        1 - t_n
     \end{bmatrix}

     \cdot

     \begin{bmatrix}
        \ln k \\
        \alpha
     \end{bmatrix}

     + \underline{e}

    &= 
     
     \begin{bmatrix}
        \ln \upsilon_1 \\
        \ln \upsilon_2 \\
        \ln \upsilon_3 \\
        ... \\
        \ln \upsilon_n
     \end{bmatrix}
\end{aligned}
```

hier fur $n = 30 \qquad$   H$[20 \times 2]$
Ergebnis: $\hat{\ln k} = 4.53  	\implies \hat{k} = e^{4,53} = 92,75 \degree C$

$k = 95 \degree C$

$\alpha = 0.02$

![](./Sources/2Modell.jpg)
## Modellgüte

```math
\begin{aligned}
    \sum_{i = 1}^{m} \hat{x_i}^2 < 10 (n -m) \sigma^2 \qquad \text{?}
\end{aligned}
```

$n = 30$, $m = 2$
$\sigma^2 = 4^2 = 16$

```math
\begin{aligned}
    \sum_{i = 1}^{m} \hat{x_i}^2 < 10 (30 - 2) 16 = 4480
\end{aligned}
```
1. Für die linerare Kennlinie $J(\hat{y} \space lim)= \sum_{i = 1}^{m} \hat{x_i}^2  = 2202$
2. Für des expo Modell $J(\hat{y} \space exp) = 631.62$