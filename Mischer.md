# Mischer

```math
\begin{aligned}
    f_1 = f_0 - \dfrac{\Delta f}{2}\\
    f_2 = f_0 + \dfrac{\Delta f}{2}\\
\end{aligned}
```

Phasenverschiebung von $\varphi_0$


```math
\begin{aligned}
    u_1(t) = U_{10} \cdot \sin[2\pi(f_0 - \dfrac{\Delta f}{2} )t
     - \dfrac{\varphi_0}{2}] \\
    u_2(t) = U_{20} \cdot \sin[2\pi(f_0 + \dfrac{\Delta f}{2} )t
     - \dfrac{\varphi_0}{2}]
\end{aligned}
```

Multiplikation der Sginale im Mischer

```math
\begin{aligned}
   \sin x \cdot \sin y = \frac{1}{2}[\cos(x - y) - \cos(x + y)] \\
   x = 2\pi f_0 \cdot t - 2\pi \frac{\Delta f}{2} \cdot t - \dfrac{\varphi_0}{2} \\
   y = 2\pi f_0 \cdot t + 2\pi \frac{\Delta f}{2} \cdot t + \dfrac{\varphi_0}{2} \\
   u_M(t) = u_1(t) \cdot u_1(t) = \dfrac{u_{10} \cdot u_{20}}{2}[\cos(-2\pi\Delta f \cdot t - \varphi_0) - \cos(4\pi f_0 \cdot t)]
\end{aligned}
```

Thiefpassfilter :
```math
\begin{aligned}
    \dot{u}_m(t) = \widehat{u} \cdot cos[2\pi\Delta f\cdot t + \varphi_0] 
\end{aligned}
```

**Schéma**
* Oszillator 1
* Oszillator 2
* Mischer 1
* Messstrecke
* Phasengleicher
* Trigger
* XOR
* Thiefpass
![mischer](./sources/mischer2.jpg)