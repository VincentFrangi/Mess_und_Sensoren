# Ultraschalldoppler-aufgabe

![](./Sources/Ultraschalldoppler-aufgabe.PNG)

**Dopplefreq**:

```math
\begin{aligned}
f_B = f_s \cdot \dfrac{1 \pm \frac{v_b}{c}}{1 \pm \frac{v_s}{c}}\\
\end{aligned}
```

## 2 Wege:

**1 Weg**: MIC stahlt ab, vs = 0

```math
\begin{aligned}
f_B = f_S \cdot (1 + \frac{v_B}{c}) \\
f_{BK} = f_{\text{MICS}} \cdot (\dfrac{v + c}{c}) \qquad (1)
\end{aligned}
```

**2 Weg**: Pratikel sind beugt und rejektion: $v_s \not= 0$ $v_B = 0$

```math
\begin{aligned}
f_B = f_S \cdot \dfrac{1}{ 1 - \frac{v_s}{c}} \\
f_{\text{MICS}} = f_{BK} \cdot \dfrac{c}{c - v} \qquad (2)
\end{aligned}
```

**(1) in (2)**
```math
\begin{aligned}
f_{\text{RICE}} = f_{\text{MICS}} \cdot \dfrac{v + c}{c} \cdot \dfrac{c}{c-v} 
= f_0 \cdot \dfrac{v +c}{c -v}\\
 \text{mit} f_0 = f_{MICS}= ?AB? - \text{Starkfrequenz}
\end{aligned}
```

**Doppleverschiebung** : $\Delta f = f_{\text{MICE}} - f_0$
```math
\begin{aligned}
\Delta f = f_0 \dfrac{v+c}{c-v}-f_0 = f_0(\dfrac{v+c}{c-v}-1)
= f_0(\dfrac{v+c}{c-v}-\dfrac{c-v}{c-v}) =
\boxed{f_0 \cdot \dfrac{2v}{c-v}}
\end{aligned}
```

$\Delta f = 2 \cdot f_0 \dfrac{v}{c}$ mit $v$ hier $=0.6$ m/s und $c = 1483$ m/s


$ v \ll c$

hier

```math
\begin{aligned}
    \Delta f = 2 \cdot 2.4 \cdot 10^6 \frac{1}{S} \cdot \frac{0.6 m/s}{1483 m/s} \cdot \cos{8\degree}\\
    \Delta f = 1.923 \text{ kHz}
\end{aligned}
```
![](./Sources/Ultraschalldoppler-aufgabe_2.PNG)