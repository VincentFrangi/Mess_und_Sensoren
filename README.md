# Mess und Sensoren

Mettre en commun les résumés et les fiches de révision

Outils :
* Je recommande Vscode pour le markdown
[doc](https://docs.gitlab.com/ee/user/markdown.html)

Math :
* [Math in markdown](https://github.com/cben/mathdown/wiki/math-in-markdown)
* [Supprorté](https://khan.github.io/KaTeX/function-support.html)


```math
a^2 = \sqrt{b^2 + c^2}
```

```math
a = \frac a b
```

## Chapitres
1. KKF Definition
2. DMS
3. LVDT