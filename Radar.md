# Radar

## Bsp FMCW Radar

* Abstand Object 5m
* Arbeits modulater 24-26 GHz
* $\Delta t_{sweep} = 1\mu S$

**Wie gross ist die Differenzfrequenz ?**

```math
\begin{aligned}
    d &= \dfrac{C}{2} \cdot \Delta t\\
    \Delta f &= d \cdot \dfrac{2}{c} \cdot \dfrac{df}{dt}\\
    &= 5m \cdot \dfrac{2}{3 \cdot 10^8 \frac{m}{s}} \cdot \dfrac{2\cdot 10^9 \dfrac{1}{s}}{1 \cdot 10^{-6}s} \\
    &= 60,7 \cdot 10^6 \dfrac{1}{s} \\
    &= 66,7 \text{ MHz}

\end{aligned}
```

## BSP US-Doppler

Ein Abwasserohr weisst die Durchflussgeschwindigkeit $ \sigma = 0,6 \frac{m}{s}$ auf. 
Ein US-Sender mit 2,4 MHz mit $\theta = 8 \degree$ gegen die Störungenrichtung verbaut

**Wie gross ist die Dopplerfrequenz ?**

```math
\begin{aligned}
    f_b = f_s \cdot \dfrac{1 \pm \frac{v_B}{C}}{1 + \frac{v_s}{C}}
    &\Rrightarrow f_b(1+\frac{v_S}{C}) = f_S \\

    f_B + f_S \cdot \dfrac{v_S}{C_w} = f_s
    &\Rrightarrow f_b \cdot \frac{v_S}{C}) = \underbrace{\Delta f}_{f_s - f_B} \\

    \cos{\theta} = \dfrac{V_{aw}}{V_m} 
    &\Rrightarrow V_{aw} = \cos{theta} \cdot V_m \\

    \Delta f = \Delta f_B \cdot \dfrac{V_{aw}\cdot \cos{\theta}}{C_w} 
    &= 24 \cdot 10^6 \frac{1}{s} \cdot \dfrac{0,6 m/s}{1483 m/s} \cdot \cos{8}
    \cong 960 \text{ Hz}
\end{aligned}
```