# LVDT

## Notwendig Elektronik

a. Erregerspannung (für den Transfo)
b. Versorgung der Primarspule / Primarspule
c. Sychrones Demodulator für Ausgangsspannung
d. Signal koditionierung
* filter
* Verstärker
* Öffsetregulierung

Fertigunge Bausteine (IC's)
* Fertige Bausteine (IC's) z.B. AD698 (Universal LVDT signal conditioner)
    LVDT Typischweise Lonearäsfehler 0.05%
    Anwendungsbereich §mm bis 0.5m

**BSP: Harmonischer Oszillator für Zylinderkondensator:**

[//]: # (insert pic)

```math
\begin{aligned}
    c_1 = Grundkapacität \\
    c_2 =  Abstimmtstecker \\
    \Delta c =  Zusatzkapazitat
\end{aligned}
```

**Thomosche Schwingugsformel**

```math
f =  \dfrac{1}{2\pi} \cdot \dfrac{1}{\sqrt{L \cdot C}} 
```

Es gitl :

```math
\begin{aligned}
    c_1 = 18pF \\
    Abstimm.
    \begin{bmatrix}
    c_2 =  90pf \\
    L =  11uH
    \end{bmatrix}
\end{aligned}
```

a) Resonanzfrquenz $f_0$ fur $\Delta$ 
```math
    \begin{aligned}
    f_0 = \dfrac{1}{2\pi} \cdot \dfrac{1}{\sqrt{L \cdot C_{ges}}} \\
    C_{ges} = C_1 + C_2 \Delta{C} = 108pF \\
    f_0 = \dfrac{1}{2\pi} \cdot \dfrac{1}
    {\sqrt{11 \cdot 10 ^{-6}H \cdot 108 \cdot 10^{-12}F}} = 4.618mHz \\
    \end{aligned}
```

b) Für kleine Änderungen ergiebt sich ein nahezu lineares Verhalten zw. $\Delta f$ und $\Delta C$ . Geben sie die linearisierte Beziehung für die rel. Freqänderung $\frac{\Delta f}{f_0}$ unter Verwendung eines Talorapprox an.

```math
    \begin{aligned}
    \Delta f \approx \frac{\delta f}{\delta c} \vert (c = c_0) \cdot \Delta C \\
    \frac{\delta f}{\delta c} = \frac{1}{2\pi} \cdot \frac{1}{\sqrt{L}} \cdot
    (-\dfrac{1}{2}) \cdot c^{-\frac{3}{2}} \\
    \Delta f \approx \dfrac{1}{2\pi} \cdot \dfrac{1}{\sqrt{L \cdot C}} \cdot 
    ( -\dfrac{1}{2} \cdot \dfrac{\Delta C}{C_0})
    \end{aligned}
```

rel. Freq-änderung :
```math
    \begin{aligned}
    \dfrac{\Delta f}{f_0} \approx \dfrac{1}{2} \cdot \dfrac{\Delta C}{C_0} \approx k \cdot \Delta C
    \end{aligned}
```


c) Geben sie für die max. Kapazitätänderung $\Delta C = 0.32pF$ die absolute Frequenzänderung $\Delta f$ an.

i) Linearisiert
```math
    \begin{aligned}
    \Delta f_{in} = f_0(\frac{1}{2}) \cdot \dfrac{0.32}{108} = - 68,841 kHz
    \end{aligned}
```
ii) mit gl.(1)
```math
    \begin{aligned}
    \Delta f_{D} = f_0(\frac{1}{2\pi \cdot \sqrt{L \cdot (C_0 + \Delta C) }})
     - \dfrac{1}{ 2\pi \cdot \sqrt{L \cdot C_0}} = - 68,825 kHz
    \end{aligned}
```
d) Rel. Fehler des abs. Frequenzämderung
```math
    \begin{aligned}
        \begin{vmatrix}
           \dfrac{\Delta f_{in} - \Delta f_{D}}{\Delta f_D}
        \end{vmatrix}
        = 0.22 \%
    \end{aligned}
```

